package test;

import org.junit.jupiter.api.*;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import static org.junit.jupiter.api.Assertions.*;

class CalculatorTest {
    private final PrintStream standardOut = System.out;
    private final ByteArrayOutputStream outputStreamCaptor = new ByteArrayOutputStream();


    @BeforeEach
    public void setUp() {
        System.setOut(new PrintStream(outputStreamCaptor));
    }

    @Test
    void testAddSuccess() {
        Calculator.add(1, 3);
        String expected = 1 + " + " + 3 + " = " + 4;
        assertTrue(expected.equals(outputStreamCaptor.toString().trim()));
    }
    @Test
    void testAddFailed() {
        Calculator.add(1, 3);
        String expected = 7 + " + " + 3 + " = " + 4;
        assertFalse(expected.equals(outputStreamCaptor.toString().trim()));
    }
    @Test
    void testSubtractSuccess() {
        Calculator.subtract(9, 4);
        String expected = 9 + " - " + 4 + " = " + 5;
        assertTrue(expected.equals(outputStreamCaptor.toString().trim()));

    }
    @Test
    void testSubtractFailes() {
        Calculator.subtract(9, 4);
        String expected = 0 + " - " + 4 + " = " + 5;
        assertFalse(expected.equals(outputStreamCaptor.toString().trim()));

    }
    @Test
    void testMultiplySuccess() {
        Calculator.multiply(9, 4);
        String expected = 9 + " * " + 4 + " = " + 36;
        assertTrue(expected.equals(outputStreamCaptor.toString().trim()));
    }

    @Test
    void testDivideSuccess() {
        Calculator.divide(36, 4);
        String expected = 36 + " / " + 4 + " = " + 9;
        assertTrue(expected.equals(outputStreamCaptor.toString().trim()));
    }

    @Test
    void testDivideFailed() {
        assertThrows(ArithmeticException.class, () -> {
            Calculator.divide(36, 0);
        });
    }

    @AfterEach
    public void setDown() {
        System.setOut(standardOut);
    }
}

